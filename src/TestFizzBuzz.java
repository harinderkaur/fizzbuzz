import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestFizzBuzz {

	@Test
	public void testInputGreaterThanZero() {
		//req1 -- red getFizzBuzz not implemented
	 FizzBuzz fb = new FizzBuzz();
	  String result = fb.getFizzBuzz(1);
	  assertEquals("1",result);
	}
	
	@Test
	public void testInputLessThanZero() {
		//req1 -- red getFizzBuzz not implemented
	 FizzBuzz fb = new FizzBuzz();
	  String result = fb.getFizzBuzz(-1);
	  assertEquals("error",result);
	}
	
	 //Test case for requirement 2
		@Test
		public void testInputIsDivisibleBy3() {
			//req3 -- red getFizzBuzz not implemented
		 FizzBuzz fb = new FizzBuzz();
		  String result = fb.getFizzBuzz(18);
		  assertEquals("fizz",result);
		}
		
		
    //Test case for requirement 3
	@Test
	public void testInputIsDivisibleBy5() {
		//req3 -- red getFizzBuzz not implemented
	 FizzBuzz fb = new FizzBuzz();
	  String result = fb.getFizzBuzz(5);
	  assertEquals("buzz",result);
	}
    //Test case for requirement 4
	@Test
	public void testInputIsDivisibleBy15() {
		//req3 -- red getFizzBuzz not implemented
	 FizzBuzz fb = new FizzBuzz();
	  String result = fb.getFizzBuzz(15);
	  assertEquals("fizzbuzz",result);
	}
	 //Test case for requirement 5
		@Test
		public void testInputIssomethingelse() {
			//req3 -- red getFizzBuzz not implemented
		 FizzBuzz fb = new FizzBuzz();
		  String result = fb.getFizzBuzz(7);
		  assertEquals("7",result);
		}
	
	
}
