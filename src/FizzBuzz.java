
public class FizzBuzz {

	public String getFizzBuzz(int i) {
	  if (i < 0)
	  {
		  return "error";
	  }
	  
	  if(i % 15 == 0)
	  {
		  return "fizzbuzz";
	  }
	  
	  if (i % 5 == 0)
	  {
		  return "buzz";
	  }
	  else if(i % 3 == 0)
	  {
		  return "fizz";
	  }
	 
		return String.valueOf(i);
	}

}
